package bcas.ap.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class JdbcConnection {
	static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://localhost:3306/emp";

	static final String USER = "root";
	static final String PASS = "";

	public static void main(String[] args) {
		Connection conn = null;
		Statement stmt = null;

		try {
			Class.forName(JDBC_DRIVER);

			System.out.println("Connecting To Database...");

			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			conn.close();
			System.out.println("Connection Successful..");
		} catch (SQLException serror) {
			serror.printStackTrace();

		} catch (ClassNotFoundException cnfe) {
			cnfe.printStackTrace();

		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException sqlex) {
			}
			try {
				if (conn != null)
					conn.close();

			} catch (SQLException se) {
				se.printStackTrace();

			}
			System.out.println("Good Bye");
		}

	}
}
